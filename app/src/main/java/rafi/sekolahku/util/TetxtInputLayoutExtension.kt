package rafi.sekolahku.util

import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.setErrorInput(errorMessage: String){
    error = errorMessage
    isErrorEnabled = true
}

fun TextInputLayout.clearErrorInput(){
    error = null
    isErrorEnabled = false
}

fun TextInputLayout.addOnTextChangedListener(onTextChanged: (newText: String) -> Unit){
    editText?.addTextChangedListener(object : TextWatcher{
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
         val userInput = s?.toString() ?:""
         onTextChanged(userInput)
        //onTextChanged(s?.toString() ?:"")
        }

        override fun afterTextChanged(s: Editable?) {

        }

    })
}