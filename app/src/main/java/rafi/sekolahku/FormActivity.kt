 package rafi.sekolahku

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_form.*
import rafi.sekolahku.util.*

 class FormActivity : AppCompatActivity() {
    private fun showLoading(show: Boolean){
        progress_circular.setVisible(show)
        save_btn.setVisible(!show)
    }
    private fun getSelectedHobbies():String{
        val selectedHobbies = ArrayList<String>()
        if (reading.isChecked) {
            selectedHobbies.add("Membaca")
        }
        if (writing.isChecked){
            selectedHobbies.add("Menulis")
        }
        if (drawing.isChecked){
            selectedHobbies.add("Menggambar")
        }
        return TextUtils.join (", ", selectedHobbies)
    }
    private fun validateFirstname(firstname: String):Boolean{
        return if (firstname.isEmpty()){
            firstname_til.setErrorInput("First name is required")
            false
        } else {
            firstname_til.clearErrorInput()
            true
        }
    }

    private fun validatePhone(phone: String):Boolean{
        return if (phone.isEmpty()){
            phone_til.setErrorInput("Phone is required")
            false
        } else {
            phone_til.clearErrorInput()
            true
        }
    }
    private fun validateInputs(): Boolean {
        var valid = true
        val firstname = first_name.text.toString()
        val phone = phone.text.toString()
        val phoneValid = validatePhone(phone)
        val firstnameValid = validateFirstname(firstname)
        if(!firstnameValid) valid =false
        if(!phoneValid) valid =false
        return valid
    }
//    private fun validateInputs():Boolean{
//        var valid = true
//        val firstname = first_name.text.toString()
//        //val lastname = last_name.text.toString()
//        val phone = phone.text.toString()
//        if (firstname.isEmpty()){
//            valid = false
//            firstname_til.setErrorInput("First name is required")
//        } else {
//            firstname_til.clearErrorInput()
//        }
////        if (lastname.isEmpty()){
////            valid = false
////            lastname_til.setErrorInput("Last name is required")
////        } else {
////            lastname_til.clearErrorInput()
////        }
//        if (phone.isEmpty()){
//            valid = false
//            phone_til.setErrorInput("Phone is required")
//        } else {
//            phone_til.clearErrorInput()
//        }
//        return valid
//    }
    private fun addNewStudent(){
    val firstname = first_name.text.toString()
    val lastname = last_name.text.toString()
    val email = email.text.toString()
    val address = address.text.toString()
    val phone = phone.text.toString()
    val selectedGender = if (gender.checkedRadioButtonId == R.id.male) "Pria" else "Wanita"
    val selectedEducation = education.selectedItem.toString()
    val selectedHobbies = getSelectedHobbies()

    val toastMessage = "Firstname : $firstname" +
            "\nLast name : $lastname" +
            "\nE-mail : $email" +
            "\nNo.Hp : $phone" +
            "\nGender : $selectedGender" +
            "\nEducation : $selectedEducation" +
            "\nHobi : $selectedHobbies" +
            "\nAlamat : $address"

    showToast(toastMessage)
    }

    private fun save() {
        val isAllInputValid = validateInputs()
        if (!isAllInputValid) {
            return
        }
        showLoading(true)
        progress_circular.visibility = View.VISIBLE
        save_btn.visibility = View.GONE
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                showLoading(false)
                addNewStudent()
            }

        }.start()
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.save_menu -> {
                save()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.form_activity_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        save_btn.setOnClickListener{
            save()
        }
        firstname_til.addOnTextChangedListener {
            validateFirstname(it)
        }
        phone_til.addOnTextChangedListener {
            validatePhone(it)
        }
//        first_name.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                validateFirstname(s?.toString() ?: "")
//            }
//
//            override fun afterTextChanged(s: Editable?) {
//
//            }
//
//        })
//        phone.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                validatePhone(s?.toString() ?: "")
//            }
//
//            override fun afterTextChanged(s: Editable?) {
//
//            }
//
//        })
    }
}