package rafi.sekolahku

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_regristration.*
import rafi.sekolahku.util.*

class RegristrationActivity : AppCompatActivity() {
    private fun validateFullname(fullname: String): Boolean {
        return if (fullname.isEmpty()) {
            fullname_til.setErrorInput("Full name is required")
            false
        } else {
            fullname_til.clearErrorInput()
            true
        }
    }

    private fun validateUsername(username: String): Boolean {
        return if (username.isEmpty()) {
            username_til.setErrorInput("Username is required")
            false
        } else {
            username_til.clearErrorInput()
            true
        }
    }

    private fun validatePassword(password: String): Boolean {
        return if (password.isEmpty()) {
            password_til.setErrorInput("Password is required")
            false
        } else if (password.length < 6) {
            password_til.setErrorInput("Password must be at least 6 digit/char")
            false
        } else {
            password_til.clearErrorInput()
            true
        }
    }

    private fun validateConfirmedPassword(confirmedpassword: String): Boolean {
        val password = password_et.text.toString()
        Log.d(TAG, "validateConfirmedPassword: pwd: $password, confrmpwd : $confirmedpassword")
        return if (confirmedpassword.isEmpty()) {
            password2_til.setErrorInput("Confirmed Password is required")
            false
        } else if (password.isNotEmpty() && password != confirmedpassword) {
            password2_til.setErrorInput("confirmed password not matched")
            false
        } else if (password.isEmpty() && confirmedpassword.isNotEmpty()){
           password2_til.setErrorInput("Password must be filled first")
           false
        }
        else {
            password2_til.clearErrorInput()
            true
        }
    }

    private fun validateInputs(): Boolean {
        var valid = true
        val fullname = full_name.text.toString()
        val username = username.text.toString()
        val confirmedpassword = password2_et.text.toString()
        val password = password_et.text.toString()
        val fullnameValid = validateFullname(fullname)
        val usernameValid = validateUsername(username)
        val confirmedpasswordValid = validateConfirmedPassword(confirmedpassword)
        val passwordValid = validatePassword(password)
        if (!fullnameValid) valid = false
        if (!usernameValid) valid = false
        if (!confirmedpasswordValid) valid = false
        if (!passwordValid) valid = false
        return valid
    }

    private fun register() {
        val isAllInputValid = validateInputs()
        return if (!isAllInputValid) {
            alertbox()
        } else {
            showToast("Register Berhasil")
        }
    }

    private fun alertbox() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Register failed!")
        builder.setMessage("Please fill all required field")
        builder.setNegativeButton("Retry", { dialogInterface: DialogInterface, i: Int -> })
        builder.show()
    }

    private fun toggleAggrement(){
        val isSelected = aggrement.isChecked
        register_btn.isEnabled = isSelected
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regristration)
        aggrement.setOnClickListener{
            toggleAggrement()
        }

        register_btn.setOnClickListener {
            register()
        }
        fullname_til.addOnTextChangedListener {
            validateFullname(it)
        }
        username_til.addOnTextChangedListener {
            validateUsername(it)
        }
        password_til.addOnTextChangedListener {
            validatePassword(it)
        }
        password2_til.addOnTextChangedListener {
            validateConfirmedPassword(it)
        }
    }
    companion object {
        private const val TAG = "RegistrationActivity"
    }
}